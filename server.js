let express = require('express');
let app = express();
let mongojs= require('mongojs');
let db=mongojs('notelist',['notelist']);
let bodyParser= require('body-parser');

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

app.get('/notelist', function(req, res){
    console.log("i recieve a get request")

    db.notelist.find(function(err, docs){
    console.log(docs);
    res.json(docs);

    });

});

app.post('/notelist', function (req, res){
    console.log(req.body);
    db.notelist.insert(req.body, function(err, doc){
        res.json(doc);
    })
});
app.delete('/notelist/:id', function(req, res){
    let id = req.params.id;
    console.log(id);
    db.notelist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

app.get('/notelist/:id', function(req, res){
    let id = req.params.id;
    console.log(id);
    db.notelist.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

app.put('/notelist/:id', function(req, res){
    let id =req.params.id;
    console.log(req.body.name);
    db.notelist.findAndModify({query:{_id: mongojs.ObjectId(id)},
        update:{$set: {name:req.body.name, email:req.body.email, notes:req.body.notes}},
        new: true}, function(err, doc){
res.json(doc);
        
    });

    });


app.listen(3000);
console.log("server running at port 3000")